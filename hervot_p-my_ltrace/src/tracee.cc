#include <iostream>
#include <unistd.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <sys/reg.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <elf.h>
#include <string.h>
#include "tracee.hh"
#include "syscall_tools.hh"

Tracee::Tracee(const std::string filename, char* const command_argv[])
    : filename_(filename),
      pid_(-1),
      is_good_(false),
      error_()
{
    pid_ = fork();
    if (pid_ == -1) // Error
        return;

    if (pid_ == 0) // In the child
    {
        ptrace(PTRACE_TRACEME, 0, 0, 0);
        if (-1 == execve(filename_.c_str(), command_argv, environ))
            exit(errno);
    }

    // Check for execve errors
    int status;
    waitpid(pid_, &status, 0);
    if (WIFEXITED(status))
    {
        error_ = strerror(WEXITSTATUS(status));
        return;
    }

    if (WIFSTOPPED(status))
        is_good_ = true;
}

Tracee::~Tracee()
{
}

bool Tracee::prepare_elf()
{
    // Map the file in memory
    fd_ = open(filename_.c_str(), 0);
    struct stat sb;
    fstat(fd_, &sb);
    void* elf = mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, fd_, 0);

    // Get the elf string table
    Elf64_Ehdr* elfh = (Elf64_Ehdr*)elf;
    Elf64_Shdr* shdr = (Elf64_Shdr*)((char*)elf + elfh->e_shoff);
    char* strtab = (char*)elf + shdr[elfh->e_shstrndx].sh_offset;

    Elf64_Sym* dynsym;
    uint16_t dynsym_size;

    Elf64_Rela* reladyn;
    uint16_t reladyn_size;

    Elf64_Plt* plt;
    uint16_t plt_size;

    char* dynstr;

    // Get the .dynsym, .rela.plt and .dynstr sections
    for (uint16_t i = 0; i < elfh->e_shnum; i++)
    {
        if (!strcmp(strtab + shdr[i].sh_name, ".dynsym"))
        {
            dynsym = (Elf64_Sym*)((char*)elf + shdr[i].sh_offset);
            dynsym_size = shdr[i].sh_size;
            dynsym_size /= shdr[i].sh_entsize;
        }
        else if (!strcmp(strtab + shdr[i].sh_name, ".rela.plt"))
        {
            reladyn = (Elf64_Rela*)((char*)elf + shdr[i].sh_offset);
            reladyn_size = shdr[i].sh_size;
            reladyn_size /= shdr[i].sh_entsize;
        }
        else if (!strcmp(strtab + shdr[i].sh_name, ".plt"))
        {
            plt = (Elf64_Plt*)((char*)elf + shdr[i].sh_offset + 0x10);
            plt_size = shdr[i].sh_size;
            plt_size /= shdr[i].sh_entsize;
        }
        else if (!strcmp(strtab + shdr[i].sh_name, ".dynstr"))
            dynstr =  (char*)elf + shdr[i].sh_offset;
    }

    // Use the .rela.plt entries to build got_entry_names.
    for (uint16_t i = 0; i < reladyn_size; i++)
    {
        // Get needed informations.
        Elf64_Addr offset = reladyn[i].r_offset;
        uint64_t sym_n = ELF64_R_SYM(reladyn[i].r_info);
        char* symbol = dynstr + dynsym[sym_n].st_name;

        // Register GOT entry
        got_entries_[offset] = symbol;
    }

    // Set breakpoints on every plt entry and build plt_entries
    for (uint16_t i = 0; i < plt_size - 1; i++)
    {
        // Plt entry
        Elf64_Addr local_addr = (Elf64_Addr)(plt + i);
        Elf64_Addr tracee_addr = (0x400000 + local_addr - (Elf64_Addr)elf);

        Elf64_Plt* entry_data = (Elf64_Plt*)local_addr;

        Elf64_Addr local_next_rip = local_addr + 8;
        Elf64_Addr tracee_next_rip = 0x400000 + local_next_rip - (Elf64_Addr)elf;

        plt_entry entry;
        entry.got_entry = (entry_data->rip_offset) + tracee_next_rip - 2;
        entry.original_inst = ptrace(PTRACE_PEEKDATA, pid_, tracee_addr, NULL);

        plt_entries_[tracee_addr] = entry;

        // And set the breapoint
        long breakpoint = (entry.original_inst & ~0xff) | 0xcc;
        ptrace(PTRACE_POKEDATA, pid_, tracee_addr, breakpoint);
    }

    return true;
}

int Tracee::get_return_code() const
{
    if (is_good_)
        return -1;
    return exit_code_;
}

/**
 * Continue the tracee until the next syscall, then print it, it's
 * parameters and it's return value.
 */
int Tracee::next_syscall()
{
    int status;

    // Continue until syscall call
    ptrace(PTRACE_SYSCALL, pid_, 0, 0);
    waitpid(pid_, &status, 0);
    if (check_exited(status))
        return -1;

    if (!(WIFSTOPPED(status) && SIGTRAP == WSTOPSIG(status)))
    {
        error_ = "Stopped but without SIGTRAP";
        return -1;
    }

    long syscall_number = ptrace(PTRACE_PEEKUSER, pid_, 8 * ORIG_RAX, 0);
    get_syscall_infos(pid_, syscall_number);

    // Continue until syscall return
    ptrace(PTRACE_SYSCALL, pid_, 0, 0);
    waitpid(pid_, &status, 0);
    if (check_exited(status))
    {
        std::cerr << "?" << std::endl;
        return -1;
    }

    long syscall_ret = ptrace(PTRACE_PEEKUSER, pid_, 8 * RAX, 0);
    std::cerr << syscall_ret << std::endl;

    return 0;
}

/**
 * Continue the tracee until the next call to a dynamically linked function,
 * then print it, it's parameters and it's return value.
 */
int Tracee::next_dyn_call()
{
    int status;

    // Continue until next breakpoint
    ptrace(PTRACE_CONT, pid_, 0, 0);
    waitpid(pid_, &status, 0);
    if (check_exited(status))
        return -1;

    if (!(WIFSTOPPED(status) && SIGTRAP == WSTOPSIG(status)))
    {
        error_ = "Didn't stop with sigtrap";
        return -1;
    }

    // Gather some usefull informations
    long rip = ptrace(PTRACE_PEEKUSER, pid_, 8 * RIP, 0) - 1;
    long rsp = ptrace(PTRACE_PEEKUSER, pid_, 8 * RSP, 0);
    long ret_addr = ptrace(PTRACE_PEEKDATA, pid_, rsp, 0);

    // Print calling informations
    std::cerr << got_entries_[plt_entries_[rip].got_entry] << "(";
    std::cerr << ") = " << std::hex;

    // Replace the breakpoint with the original instruction
    long old_inst = plt_entries_[rip].original_inst;
    ptrace(PTRACE_POKEDATA, pid_, rip, old_inst);
    ptrace(PTRACE_POKEUSER, pid_, 8 * RIP, rip);

    // Execute that original instruction
    ptrace(PTRACE_SINGLESTEP, pid_, 0, 0);
    waitpid(pid_, &status, 0);
    if (check_exited(status))
        return -1;
    if (!(WIFSTOPPED(status) && SIGTRAP == WSTOPSIG(status)))
    {
        error_ = "Didn't stop with sigtrap";
        return -1;
    }

    // Set the breakpoint again
    ptrace(PTRACE_POKEDATA, pid_, rip, (old_inst & ~0xff) | 0xcc);

    // Set a breakpoint at the return address
    long post_ret_inst = ptrace(PTRACE_PEEKDATA, pid_, ret_addr, 0);
    ptrace(PTRACE_POKEDATA, pid_, ret_addr, (post_ret_inst & ~0xff) | 0xcc);

    // Continue until breakpoint
    ptrace(PTRACE_CONT, pid_, 0, 0);
    waitpid(pid_, &status, 0);
    if (check_exited(status))
        return -1;

    if (!(WIFSTOPPED(status) && SIGTRAP == WSTOPSIG(status)))
    {
        error_ = "Didn't stop with sigtrap";
        return -1;
    }

    // Print the return value
    std::cerr << ptrace(PTRACE_PEEKUSER, pid_, 8 * RAX, 0) << std::endl;

    // Replace the breakpoint with the original instruction
    rip = ptrace(PTRACE_PEEKUSER, pid_, 8 * RIP, 0) - 1;
    ptrace(PTRACE_POKEDATA, pid_, ret_addr, post_ret_inst);
    ptrace(PTRACE_POKEUSER, pid_, 8 * RIP, rip);

    return 0;
}

/**
 * Check if the tracee has exited
 * status status given by waitpid
 */
bool Tracee::check_exited(const int status)
{
    if (!is_good_ || WIFEXITED(status))
    {
        is_good_ = false;
        pid_ = -1;
        exit_code_ = WEXITSTATUS(status);
        return true;
    }
    return false;
}

bool Tracee::is_good() const
{
    return is_good_;
}

std::string Tracee::get_error() const
{
    return error_;
}
