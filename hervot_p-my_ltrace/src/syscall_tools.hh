#ifndef SYSCALL_TOOLS_HH
# define SYSCALL_TOOLS_HH

# include <unistd.h>

void get_syscall_infos(pid_t pid, long syscall);

#endif /* !SYSCALL_TOOLS_HH */
