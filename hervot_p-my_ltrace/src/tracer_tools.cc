#include <iostream>
#include <unistd.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include "tracer_tools.hh"

/**
 * Get the position of a null byte in a memory word (sizeof (long)).
 * If no null byte is found, -1 is returned
 */
static int find_null(char* word)
{
    for (size_t i = 0; i < sizeof (long); ++i)
        if ('\0' == word[i])
            return i;
    return -1;
}

/**
 * Replace every newline in a string by the (escaped) string "\n"
 */
static void replace_newline(std::string& str)
{
    for (size_t i = 0; i < str.length(); ++i)
    {
        if ('\n' == str[i])
        {
            str[i] = '\\';
            str.insert(i + 1, "n");
        }
    }
}

/**
 * Get a string in the memory of a ptraced process.
 * The process memory is roamed until a null byte is found or the acumulated
 * string exceed max_len
 */
static std::string peek_string(pid_t pid, void* addr, size_t max_len)
{
    (void)max_len;
    std::string res("");
    long raw = ptrace(PTRACE_PEEKDATA, pid, addr, 0);
    char* raw_s = (char*)&raw;
    int null_pos = find_null(raw_s);

    while (-1 == null_pos && res.length() < max_len)
    {
        res.append(raw_s, sizeof (long));
        addr = (char*)addr + sizeof (long);
        raw = ptrace(PTRACE_PEEKDATA, pid, addr, 0);
        null_pos = find_null(raw_s);
    }
    if (-1 == null_pos)
        res.append(raw_s, sizeof (long));
    else
        res.append(raw_s, null_pos);

    replace_newline(res);
    return res;
}

static unsigned long long get_reg(struct user_regs_struct regs, int arg_n)
{
    switch (arg_n)
    {
        case 0: return regs.rdi;
        case 1: return regs.rsi;
        case 2: return regs.rdx;
        case 3: return regs.r10;
        case 4: return regs.r8;
        case 5: return regs.r9;
        default: return -1;
    }
}

void print_argument(pid_t pid,
                    function_info& function,
                    struct user_regs_struct regs,
                    size_t arg_n)
{
    unsigned long long val = get_reg(regs, arg_n);
    enum function_arg_type type = function.arg_types[arg_n];

    if (function.arg_names.size() > arg_n)
        std::cerr << function.arg_names[arg_n] << " = ";
    if (INT == type)
        std::cerr << val;
    else if (STRING == type)
    {
        std::string val_s = peek_string(pid, (void*)val, TRACER_MAX_STRING_LEN);
        std::cerr << '"' << val_s << "\"";
        if (val_s.length() > TRACER_MAX_STRING_LEN)
            std::cerr << "...";
    }
    else
    {
        std::ios state(nullptr);
        state.copyfmt(std::cerr);

        std::cerr << std::hex;
        std::cerr << "0x" << val;

        std::cerr.copyfmt(state);
    }
}
