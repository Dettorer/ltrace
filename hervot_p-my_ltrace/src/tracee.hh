#ifndef TRACEE_HH
# define TRACEE_HH

# include <unistd.h>
# include <string>
# include <elf.h>
# include <map>

typedef struct {
    long            : 8 * 2;
    long rip_offset : 8 * 3;
    long fill;
} Elf64_Plt;

typedef struct {
    Elf64_Addr got_entry;
    long original_inst;
} plt_entry;

class Tracee
{
public:
    Tracee(const std::string filename, char* const command_argv[]);
    Tracee(int pid);
    virtual ~Tracee();

    bool is_good() const;
    std::string get_error() const;
    bool check_exited(const int status);
    int get_return_code() const;

    // strace utility
    int next_syscall();

    // ltrace utility
    bool prepare_elf();
    int next_dyn_call();

private:
    const std::string filename_; // TODO
    pid_t pid_;
    int fd_;
    bool is_good_;
    std::string error_;
    int exit_code_;

    std::map<Elf64_Addr, std::string> got_entries_;
    std::map<Elf64_Addr, plt_entry> plt_entries_;

};

#endif /* !TRACEE_HH */
