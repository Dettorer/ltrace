#ifndef TRACER_TOOLS_HH
# define TRACER_TOOLS_HH

# include <unistd.h>
# include <string>
# include <vector>

# ifndef TRACER_MAX_STRING_LEN
#  define TRACER_MAX_STRING_LEN 32
# endif

enum function_arg_type
{
    POINTER,
    STRING,
    INT
};

struct function_info
{
    std::string name;
    int nargs;

    std::vector<enum function_arg_type> arg_types;
    std::vector<std::string> arg_names;

    function_info(std::string name, int nargs)
        : name(name), nargs(nargs)
    {
    }
    function_info()
    {
    }
};

void print_argument(pid_t pid,
                    function_info& function,
                    struct user_regs_struct regs,
                    size_t arg_n);

#endif /* !TRACER_TOOLS_HH */
