#include <iostream>
#include <map>
#include <asm/unistd.h>
#include <sys/user.h>
#include <sys/ptrace.h>
#include "syscall_tools.hh"
#include "tracer_tools.hh"

// Map syscall numbers to it's structure.
static std::map<long, struct function_info> syscall_map;
static void init_map()
{
#define X(NAME, NARGS, ...)                                     \
    syscall_map[__NR_ ## NAME] = function_info(#NAME, NARGS);   \
    syscall_map[__NR_ ## NAME].arg_types = { __VA_ARGS__ };
#define Y(NAME, ...)                                            \
    syscall_map[__NR_ ## NAME].arg_names = { __VA_ARGS__ };
#include "syscall_tools.def"
#undef X
#undef Y
}

static bool known_syscall(long syscall)
{
    return syscall_map.count(syscall);
}

void get_syscall_infos(pid_t pid, long syscall_number)
{
    if (syscall_map.empty())
        init_map();

    if (!known_syscall(syscall_number))
    {
        std::cerr << "Syscall " << syscall_number << " = ";
        return;
    }

    struct function_info syscall = syscall_map[syscall_number];

    struct user_regs_struct tracee_regs;
    ptrace(PTRACE_GETREGS, pid, 0, &tracee_regs);

    std::cerr << syscall.name << "(";
    for (int i = 0; i < syscall.nargs; ++i)
    {
        print_argument(pid, syscall, tracee_regs, i);
        if (i < syscall.nargs - 1)
            std::cerr << ", ";
    }

    std::cerr << ") = ";
}
