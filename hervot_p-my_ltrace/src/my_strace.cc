#include <iostream>
#include "tracee.hh"

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        std::cerr << "Usage: " << argv[0] << "command [arg ...]" << std::endl;
        return 2;
    }

    Tracee tracee(argv[1], argv + 1);

    if (!tracee.is_good())
    {
        std::cerr << argv[0] << ": error, " << tracee.get_error() << std::endl;
        return 1;
    }

    while (tracee.is_good())
        tracee.next_syscall();

    std::cerr << "+++ exited with " << tracee.get_return_code() << " +++";
    std::cerr << std::endl;

    return 0;
}
